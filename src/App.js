/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

function App() {
  const [state, setstate] = useState(1);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-lgo" alt="logo" />
        <a onMouseOver={() => {setstate(state+1)}} onClick={() => {setstate(state + 1)}}
          style={{border: "1px"}}>Click me!</a>
        <h2>{state}</h2>
      </header>
    </div>
  );
}

export default App;
